package com.axis.seed

import io.kotlintest.assertSoftly
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.bson.Document
import kotlin.streams.toList

class JsonGeneratorTests : StringSpec({

    "should generate json using template for string field value" {
        val jsonTemplate = """
            {
                "city": "[(${'$'}{props['City Unique ID']})]"
            }
        """.trimMargin().trimIndent()

        val record = mapOf("City Unique ID" to "mumbai")
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            document.getString("city") shouldBe "mumbai"
        }
    }

    "should generate json using template for integer value" {
        val jsonTemplate = """
            {
                "minTenure": [(${'$'}{props['Tenure Min']})]
            }
        """.trimMargin().trimIndent()

        val record = mapOf("Tenure Min" to "10")
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            document.getInteger("minTenure") shouldBe 10
        }
    }

    "should generate json using template for float value" {
        val jsonTemplate = """
            {
                "rateOfInterest": [(${'$'}{props['Interest Rate']})]
            }
        """.trimMargin().trimIndent()

        val record = mapOf("Interest Rate" to "9.25")
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            document.getDouble("rateOfInterest") shouldBe 9.25
        }
    }

    "should generate json using template for boolean value" {
        val jsonTemplate = """
            {
                "active": [(${'$'}{props['Active']})]
            }
        """.trimMargin().trimIndent()

        val record = mapOf("Active" to "true")
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            document.getBoolean("active") shouldBe true
        }
    }

    "should generate json using template for date value" {
        val jsonTemplate = """
            {
                "startDate": ISODate("[(${'$'}{props['Start Date']})]T00:00:00.000+0530")
            }
        """.trimMargin().trimIndent()

        val record = mapOf("Start Date" to "2018-10-10")
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            document.getDate("startDate").toInstant().epochSecond shouldBe 1539109800L  // 2018-10-10T00:00:00.000+0530
        }
    }

    "should generate json using template for nested value" {
        val jsonTemplate = """
            {
                "group": "4w-personal",
                "city": "[(${'$'}{props['City Unique ID']})]",

                "data": {
                    "channel": "[(${'$'}{props['Channel']})]",
                }
            }
        """.trimMargin().trimIndent()

        val record = mapOf("City Unique ID" to "mumbai", "Channel" to "Mobile Banking")
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            document.getString("group") shouldBe "4w-personal"
            document.getString("city") shouldBe "mumbai"

            val data = document.getValue("data") as Document
            data.getString("channel") shouldBe "Mobile Banking"
        }
    }

    "should generate json using template with default value true when data is not present" {
        val jsonTemplate = """
            {
                "active": [(${'$'}{props['Active']}?: true)]
            }
        """.trimMargin().trimIndent()

        val record = emptyMap<String, String>()
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            document.getBoolean("active") shouldBe true
        }

    }

    "should generate json using template with if condition" {
        val jsonTemplate = """
            {
                [#th:block th:if="${'$'}{props.get('Active') != null}" ]
                    "active": [(${'$'}{props['Active']})]
                [/th:block]
            }
        """.trimMargin().trimIndent()

        val record = emptyMap<String, String>()
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            assert(document.getString("active") == null)
        }

    }

    "should generate json using template with array value" {
        val jsonTemplate = """
            {
                "cities": [
                    [#th:block th:each="city, iterStat : ${'$'}{props.get('City')}" ]
                        "[#th:block th:utext="${'$'}{city}" /]" [#th:block th:utext="!${'$'}{iterStat.last} ? ',' : _ " /]
                    [/th:block]
                ]
            }
        """.trimMargin().trimIndent()

        val record = mapOf("City" to listOf("mumbai", "pune"))
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            (document.getValue("cities") as List<String>) shouldBe listOf("mumbai", "pune")
        }

    }

    "should generate json using template with array objects" {
        val jsonTemplate = """
            {
                "cities": [
                    [#th:block th:each="item, iterStat : ${'$'}{props.get('cities')}" ]
                        {
                            "city":  "[#th:block th:utext="${'$'}{item['City']}" /]",
                            "state":  "[#th:block th:utext="${'$'}{item['State']}" /]"

                        }[#th:block th:utext="!${'$'}{iterStat.last} ? ',' : _ " /]
                    [/th:block]
                ]
            }
        """.trimMargin().trimIndent()

        val record = mapOf(
            "cities" to listOf(
                mapOf("City" to "mumbai", "State" to "MH"),
                mapOf("City" to "bangalore", "State" to "KA")
            )
        )
        val jsonRecords = JsonGenerator(jsonTemplate).generate(listOf(record)).toList()

        jsonRecords.size shouldBe 1
        assertSoftly {
            val document = Document.parse(jsonRecords[0])
            val cities = document.getValue("cities") as List<String>
            var city = cities[0] as Document
            city.getString("city") shouldBe "mumbai"
            city.getString("state") shouldBe "MH"
            city = cities[1] as Document
            city.getString("city") shouldBe "bangalore"
            city.getString("state") shouldBe "KA"
        }

    }



})