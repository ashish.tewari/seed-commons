package com.axis.seed

import com.mongodb.client.MongoClients
import io.kotlintest.assertSoftly
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.bson.Document

class CsvImporterTests : StringSpec({
    "!read and import csv file with multiple records" {

        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("masters")
        val collection = database.getCollection("configs")
        collection.drop()


        CsvImporter("interest-rates").import()


        val documents = collection.find().toList()
        documents.size shouldBe 2
        assertSoftly {
            var document = documents[0]
            document.getString("group") shouldBe "4w-personal"
            document.getString("name") shouldBe "interest-rate"

            var data = document.getValue("details") as Document
            data.getString("city") shouldBe "mumbai"
            data.getString("channel") shouldBe "Internet Banking"
            data.getInteger("minTenure") shouldBe 0
            data.getInteger("maxTenure") shouldBe 2
            data.getDouble("rateOfInterest") shouldBe 9.75

            document.getBoolean("active") shouldBe true
            document.getDate("startDate").toInstant().epochSecond shouldBe 1539109800L  // 2018-10-10T00:00:00.000+0530
            document.getDate("endDate").toInstant().epochSecond shouldBe 1540060199L    // 2018-20-10T23:59:59.000+0530

            document = documents[1]
            document.getString("group") shouldBe "4w-personal"
            document.getString("name") shouldBe "interest-rate"

            data = document.getValue("details") as Document
            data.getString("city") shouldBe "pune"
            data.getString("channel") shouldBe "Mobile Banking"
            data.getInteger("minTenure") shouldBe 10
            data.getInteger("maxTenure") shouldBe 20
            data.getDouble("rateOfInterest") shouldBe 9.25

            document.getBoolean("active") shouldBe false
            document.getDate("startDate").toInstant().epochSecond shouldBe 1546281000L  // 2019-01-01T00:00:00.000+0530
            document.getDate("endDate").toInstant().epochSecond shouldBe 1577816999L    // 2019-12-31T23:59:59.000+0530
        }
    }

    "!read and load environment specific record file" {
        class MockSystemWrapperImpl: SystemWrapper {
            override fun getenv(name: String): String? {
                return "PROD"
            }
        }

        val environmentAwareRecordFileLoader = EnvironmentAwareFileLoader(MockSystemWrapperImpl())
        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("masters")
        val collection = database.getCollection("masters")
        collection.drop()
        CsvImporter("pincode-mappings", environmentAwareRecordFileLoader).import()

        val documents = collection.find().toList()
        documents.size shouldBe 1
        var document = documents[0]
        val data = document.getValue("details") as Document
        data.getString("finoneCountryCode") shouldBe "456"
    }

    "!read environment specific data file" {
        class MockSystemWrapperImpl: SystemWrapper {
            override fun getenv(name: String): String? {
                return "Uat"
            }
        }

        val environmentAwareRecordFileLoader = EnvironmentAwareFileLoader(MockSystemWrapperImpl())
        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("masters")
        val collection = database.getCollection("masters")
        collection.drop()
        CsvImporter("pincode-mappings", environmentAwareRecordFileLoader).import()

        val documents = collection.find().toList()
        documents.size shouldBe 1
        var document = documents[0]
        val data = document.getValue("details") as Document
        data.getString("finoneCountryCode") shouldBe "111"
    }
})