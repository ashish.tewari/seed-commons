package com.axis.seed

import com.mongodb.client.MongoClients
import io.kotlintest.assertSoftly
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.bson.Document

class JsonImporterTests : StringSpec() {

    //override fun listeners(): List<TestListener> = listOf(EmbedMongoDbListener)

    init {
        "!read and import json file with multiple records" {

            val client = MongoClients.create("mongodb://localhost:27017")
            val database = client.getDatabase("masters")
            val collection = database.getCollection("configs")
            collection.drop()


            JsonImporter("master-configs").import()
            JsonImporter("credit-line-template").import()

            val documents = collection.find().toList()
            documents.size shouldBe 2
            assertSoftly {
                var document = documents[0]
                document.getString("group") shouldBe "4w-personal"
                document.getString("name") shouldBe "default-city"
                var value = document.getValue("value") as Document
                value.getString("uniqueID") shouldBe "mumbai"
                value.getString("displayName") shouldBe "Mumbai"

                document = documents[1]
                document.getString("group") shouldBe "4w-personal"
                document.getString("name") shouldBe "loan-slider"
                value = document.getValue("value") as Document
                val tenure = value.getValue("tenure") as Document
                tenure.getInteger("min") shouldBe 1
                tenure.getInteger("max") shouldBe 7
                tenure.getInteger("step") shouldBe 1
                val loanAmount = value.getValue("loanAmount") as Document
                loanAmount.getInteger("min") shouldBe 100000
                loanAmount.getInteger("step") shouldBe 10000

            }
        }

        "!read and import json file with json record mapping" {

            val client = MongoClients.create("mongodb://localhost:27017")
            val database = client.getDatabase("masters")
            val collection = database.getCollection("configs")
            collection.drop()


            JsonImporter("makes").import()


            val documents = collection.find().toList()
            documents.size shouldBe 2
            assertSoftly {
                var document = documents[0]
                document.getString("type") shouldBe "4w-personal"
                document.getString("subType") shouldBe "make"
                document.getString("uniqueId") shouldBe "maruti"
                var details = document.getValue("details") as Document
                details.getString("fullName") shouldBe "Maruti"
                details.getString("shortName") shouldBe "Maruti"
                var images = details.getValue("images") as List<Document>
                images[0].getString("url") shouldBe "maruti_1920x1080.png"

                document = documents[1]
                document.getString("type") shouldBe "4w-personal"
                document.getString("subType") shouldBe "make"
                document.getString("uniqueId") shouldBe "tata"
                details = document.getValue("details") as Document
                details.getString("fullName") shouldBe "Tata"
                details.getString("shortName") shouldBe "Tata"
                images = details.getValue("images") as List<Document>
                images[0].getString("url") shouldBe "tata_1920x1080.png"


            }
        }


        "!read and import json file with base64 encoded file record mapping" {

            val client = MongoClients.create("mongodb://localhost:27017")
            val database = client.getDatabase("masters")
            val collection = database.getCollection("templates")
            collection.drop()


            JsonImporter("templates").import()


            val documents = collection.find().toList()
            documents.size shouldBe 2
            assertSoftly {
                var document = documents[0]
                document.getString("group") shouldBe "4w-personal"
                document.getString("name") shouldBe "SanctionLetter"
                var templates = document.getValue("templates") as List<Document>
                templates[0].getString("name") shouldBe "SanctionLetter"
                templates[0].getString("content") shouldBe "c2FuY3Rpb24tbGV0dGVyLWxpbmUtMQpzYW5jdGlvbi1sZXR0ZXItbGluZS0yCnNhbmN0aW9uLWxldHRlci1saW5lLTM="
                templates[0].getString("output") shouldBe "pdf"

                document = documents[1]
                document.getString("group") shouldBe "4w-personal"
                document.getString("name") shouldBe "SanctionSuccessful_EMAIL"
                templates = document.getValue("templates") as List<Document>
                templates[0].getString("name") shouldBe "subject"
                templates[0].getString("content") shouldBe "c2FuY3Rpb24tZW1haWwtc3ViamVjdA=="
                templates[0].getString("output") shouldBe "text"
                templates[1].getString("name") shouldBe "body"
                templates[1].getString("content") shouldBe "c2FuY3Rpb24tZW1haWwtYm9keQ=="
                templates[1].getString("output") shouldBe "html"


            }
        }

        "!read environment specific data file" {

            class MockSystemWrapperImpl : SystemWrapper {
                override fun getenv(name: String): String? {
                    return "PrOd"
                }
            }

            val client = MongoClients.create("mongodb://localhost:27017")
            val database = client.getDatabase("masters")
            val collection = database.getCollection("configs")
            collection.drop()


            JsonImporter("master-configs", EnvironmentAwareFileLoader(MockSystemWrapperImpl())).import()


            val documents = collection.find().toList()
            documents.size shouldBe 2
            assertSoftly {
                var document = documents[0]
                document.getString("group") shouldBe "4w-personal"
                document.getString("name") shouldBe "default-city"
                var value = document.getValue("value") as Document
                value.getString("uniqueID") shouldBe "mumbai"
                value.getString("displayName") shouldBe "Mumbai"

                document = documents[1]
                document.getString("group") shouldBe "4w-personal"
                document.getString("name") shouldBe "loan-slider"
                value = document.getValue("value") as Document
                val tenure = value.getValue("tenure") as Document
                tenure.getInteger("min") shouldBe 2
                tenure.getInteger("max") shouldBe 8
                tenure.getInteger("step") shouldBe 2
                val loanAmount = value.getValue("loanAmount") as Document
                loanAmount.getInteger("min") shouldBe 200000
                loanAmount.getInteger("step") shouldBe 20000

            }
        }

    }
}