package com.axis.seed

import com.mongodb.client.MongoClients
import io.kotlintest.shouldBe
import org.junit.jupiter.api.Disabled

class EmployersIndexingTests {


    @Disabled
    fun `should create index`() {

        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("masters")
        val collection = database.getCollection("masters")
        collection.drop()
        collection.dropIndexes()

        CsvImporter("employers").import()


        val documents = collection.find().toList()
        documents.size shouldBe 2


        val indexes = collection.listIndexes()

        indexes.count() shouldBe 2

        val firstKeys = indexes.first()["key"] as Map<String, Any>
        firstKeys["_id"] shouldBe 1

        val secondIndexKeys = indexes.elementAt(1)["key"] as Map<String, Any>
        secondIndexKeys["_fts"] shouldBe "text"

        val secondIndexWeights = indexes.elementAt(1)["weights"] as Map<String, Any>
        secondIndexWeights["details.name"] shouldBe 1

    }

}