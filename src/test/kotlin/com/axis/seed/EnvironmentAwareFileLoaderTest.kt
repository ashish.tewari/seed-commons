package com.axis.seed

import io.kotlintest.shouldBe
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class EnvironmentAwareFileLoaderTest {

    private lateinit var system: SystemWrapper
    private lateinit var fileReader: FileReader

    companion object {
        const val ACTIVE_ENVIRONMENT = "spring.profiles.active"
    }

    @BeforeEach
    fun setUp() {
        system = mockk()
        fileReader = mockk()
    }


    @Test
    fun shouldReturnDefaultFileNameInAbsenceOfEnv() {
        every { system.getenv(ACTIVE_ENVIRONMENT) } returns null
        every { fileReader.exists("/pincode/record.json") } returns true
        val fileLoader = EnvironmentAwareFileLoader(system, fileReader)

        fileLoader.getFileToLoad("pincode", "record.json") shouldBe "record.json"
    }

    @Test
    fun shouldReturnEnvSpecificFileName() {
        every { system.getenv(ACTIVE_ENVIRONMENT) } returns "PROD"
        every { fileReader.exists("/pincode/record-prod.json") } returns true
        val fileLoader = EnvironmentAwareFileLoader(system, fileReader)

        fileLoader.getFileToLoad("pincode", "record.json") shouldBe "record-prod.json"
    }

    @Test
    fun shouldReturnDefaultFileIfEnvSpecificFileDoesntExistInTheFolder() {
        every { system.getenv(ACTIVE_ENVIRONMENT) } returns "PROD"
        every { fileReader.exists("/pincode/record-prod.json") } returns false
        every { fileReader.exists("/pincode/record.json") } returns true
        val fileLoader = EnvironmentAwareFileLoader(system, fileReader)

        fileLoader.getFileToLoad("pincode", "record.json") shouldBe "record.json"
    }

    @Test
    fun shouldReturnNullIfNoRecordFileExistsAtTheGivenFolder() {
        every { system.getenv(ACTIVE_ENVIRONMENT) } returns "PROD"
        every { fileReader.exists("/pincode/record-prod.json") } returns false
        every { fileReader.exists("/pincode/record.json") } returns false
        val fileLoader = EnvironmentAwareFileLoader(system, fileReader)

        fileLoader.getFileToLoad("pincode", "record.json") shouldBe null
    }
}