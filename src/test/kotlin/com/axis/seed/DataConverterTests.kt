package com.axis.seed

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import java.util.*

class DataConverterTests : StringSpec({

    "toUniqueId for value MUMBAI" {
        DataConverter().toUniqueId("MUMBAI") shouldBe "mumbai"
    }

    "toUniqueId for value TWO_WORDS" {
        DataConverter().toUniqueId("TWO_WORDS") shouldBe "two_words"
    }


    "fileAsBase64 for templates/templates/sanction-letter.md as multiline string" {
        val value = """sanction-letter-line-1
sanction-letter-line-2
sanction-letter-line-3""".trimIndent()

        val result = DataConverter().fileAsBase64("templates/templates/sanction-letter.md")
        String(Base64.getDecoder().decode(result.toByteArray())) shouldBe value

    }

    "fileAsBase64 for templates/templates/sanction-email-subject.txt as single line string" {

        val result = DataConverter().fileAsBase64("templates/templates/sanction-email-subject.txt")
        String(Base64.getDecoder().decode(result.toByteArray())) shouldBe "sanction-email-subject"

    }
})