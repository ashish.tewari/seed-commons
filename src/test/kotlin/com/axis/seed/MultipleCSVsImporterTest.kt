package com.axis.seed

import com.mongodb.client.MongoClients
import io.kotlintest.assertSoftly
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.bson.Document

class MultipleCSVsImporterTest : StringSpec({
    "!should import multiple csv files from a folder" {
        class MockSystemWrapperImpl: SystemWrapper {
            override fun getenv(name: String): String? {
                return "prod"
            }
        }

        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("assets")
        val collection = database.getCollection("variantPrices")
        collection.drop()

        MultipleCSVsImporter(
            "4w-personal-asset-variant-prices",
            EnvironmentAwareFileLoader(MockSystemWrapperImpl())
        ).import()

        val documents = collection.find().toList()
        documents.size shouldBe 4

        assertSoftly {
            val variant238 = documents.findLast { it.getString("variantId") == "238" }!!
            variant238.getString("cityId") shouldBe "430"
            val pricingDetails = variant238.getValue("details") as Document

            pricingDetails.getString("insuranceAmount") shouldBe "36036"
            pricingDetails.getString("rtoCharges") shouldBe "0"
            pricingDetails.getString("exShowRoomPrice") shouldBe "655638"

        }

        assertSoftly {
            val variant242 = documents.findLast { it.getString("variantId") == "243" }!!
            variant242.getString("cityId") shouldBe "430"
            val pricingDetails = variant242.getValue("details") as Document

            pricingDetails.getString("insuranceAmount") shouldBe "16650"
            pricingDetails.getString("rtoCharges") shouldBe "123"
            pricingDetails.getString("exShowRoomPrice") shouldBe "278371"

        }
    }
    "!should import multiple csv files from a env specific folder" {
        class MockSystemWrapperImpl: SystemWrapper {
            override fun getenv(name: String): String? {
                return "test"
            }
        }

        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("assets")
        val collection = database.getCollection("variantPrices")
        collection.drop()

        MultipleCSVsImporter(
            "4w-personal-asset-variant-prices",
            EnvironmentAwareFileLoader(MockSystemWrapperImpl())
        ).import()

        val documents = collection.find().toList()
        documents.size shouldBe 4

        assertSoftly {
            val variant238 = documents.findLast { it.getString("variantId") == "238" }!!
            variant238.getString("cityId") shouldBe "431"
            val pricingDetails = variant238.getValue("details") as Document

            pricingDetails.getString("insuranceAmount") shouldBe "3606"
            pricingDetails.getString("rtoCharges") shouldBe "0"
            pricingDetails.getString("exShowRoomPrice") shouldBe "655638"

        }

        assertSoftly {
            val variant242 = documents.findLast { it.getString("variantId") == "243" }!!
            variant242.getString("cityId") shouldBe "432"
            val pricingDetails = variant242.getValue("details") as Document

            pricingDetails.getString("insuranceAmount") shouldBe "650"
            pricingDetails.getString("rtoCharges") shouldBe "123"
            pricingDetails.getString("exShowRoomPrice") shouldBe "278371"

        }
    }
}
)
