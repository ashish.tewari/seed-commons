package com.axis.seed

import com.mongodb.client.MongoClients
import io.kotlintest.assertSoftly
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec

class DataVersioningTest : StringSpec({

    "should insert version in the version_log collection if a new version is defined"{
        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("version_master")
        val collection = database.getCollection("version_rates")
        val versionCollection = database.getCollection("version_log")
        collection.drop()
        versionCollection.drop()

        CsvImporter("version").import()

        val versions = versionCollection.find().toList()
        val versionRates = collection.find().toList()

        versions.size shouldBe 1
        versionRates.size shouldBe 2

        assertSoftly {
            var version = versions[0]

            version.getString("collectionName") shouldBe "version-version_rates"
            version.getString("version") shouldBe "1.0.1"
        }
    }

    "should not make any update if the version is already present in the version_log collection"{
        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("version_master")
        val collection = database.getCollection("version_rates")
        val versionCollection = database.getCollection("version_log")
        collection.drop()
        versionCollection.drop()

        CsvImporter("version").import()

        var versionRates = collection.find().toList()
        versionRates.size shouldBe 2

        collection.drop()
        CsvImporter("version").import()

        versionRates = collection.find().toList()

        versionRates.size shouldBe 0
    }

    "should insert version as null in the version_log collection if version is not defined"{
        val client = MongoClients.create("mongodb://localhost:27017")
        val database = client.getDatabase("masters")
        val collection = database.getCollection("masters")
        val versionCollection = database.getCollection("version_log")
        collection.drop()
        versionCollection.drop()

        CsvImporter("employers").import()

        val versions = versionCollection.find().toList()
        val employers = collection.find().toList()

        versions.size shouldBe 1
        employers.size shouldBe 2

        assertSoftly {
            var version = versions[0]

            version.getString("collectionName") shouldBe "employers-masters"
            version.getString("version") shouldBe null
        }
    }
})