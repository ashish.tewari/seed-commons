package com.axis.seed

import de.flapdoodle.embed.mongo.MongodExecutable
import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.runtime.Network
import io.kotlintest.Description
import io.kotlintest.TestResult
import io.kotlintest.extensions.TestListener

object EmbedMongoDbListener : TestListener {
    private lateinit var mongodExecutable: MongodExecutable

    override fun beforeTest(description: Description) {
        val starter = MongodStarter.getDefaultInstance()

        val mongodConfig = MongodConfigBuilder()
            .version(Version.Main.PRODUCTION)
            .net(Net("localhost", 27017, Network.localhostIsIPv6()))
            .build()

        mongodExecutable = starter.prepare(mongodConfig)
        mongodExecutable.start()
        Thread.sleep(2000)
    }

    override fun afterTest(description: Description, result: TestResult) {
        mongodExecutable.stop()
    }
}