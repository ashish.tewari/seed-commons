package com.axis.seed

import io.kotlintest.assertSoftly
import io.kotlintest.matchers.maps.shouldNotContainKey
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec

class CsvDataParserTests : StringSpec({

    "should parse and generate csv data map for given single perfect csv record" {
        val csvHeader = "City Unique ID,Tenure Min"
        val csvRecords = listOf("mumbai,10")

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 1
        assertSoftly {
            dataRecords[0]["City Unique ID"] shouldBe "mumbai"
            dataRecords[0]["Tenure Min"] shouldBe "10"
        }
    }

    "should parse and generate csv data map for given multiple perfect csv record" {
        val csvHeader = "City Unique ID,Tenure Min"
        val csvRecords = listOf("mumbai,10", "pune,20")

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 2
        assertSoftly {
            dataRecords[0]["City Unique ID"] shouldBe "mumbai"
            dataRecords[0]["Tenure Min"] shouldBe "10"

            dataRecords[1]["City Unique ID"] shouldBe "pune"
            dataRecords[1]["Tenure Min"] shouldBe "20"
        }
    }

    "should parse and generate csv data map for given csv record with spaces" {
        val csvHeader = "City Unique ID,Tenure Min, Tenure Max"
        val csvRecords = listOf("mumbai ,10 ,20 ")

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 1
        assertSoftly {
            dataRecords[0]["City Unique ID"] shouldBe "mumbai"
            dataRecords[0]["Tenure Min"] shouldBe "10"
            dataRecords[0]["Tenure Max"] shouldBe "20"
        }
    }

    "should parse and generate csv data map for given csv record with quoted string" {
        val csvHeader = "City Unique ID,Tenure Min, Tenure Max"
        val csvRecords = listOf("\"mumbai\",10 ,20 ")

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 1
        assertSoftly {
            dataRecords[0]["City Unique ID"] shouldBe "mumbai"
            dataRecords[0]["Tenure Min"] shouldBe "10"
            dataRecords[0]["Tenure Max"] shouldBe "20"
        }
    }

    "should parse and generate csv data map for given csv record with inline comma" {
        val csvHeader = "City Unique ID,Tenure Min, Tenure Max"
        val csvRecords = listOf("\"my, mumbai\",10 ,\"20\"")

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 1
        assertSoftly {
            dataRecords[0]["City Unique ID"] shouldBe "my, mumbai"
            dataRecords[0]["Tenure Min"] shouldBe "10"
            dataRecords[0]["Tenure Max"] shouldBe "20"
        }
    }

    "handle empty values as missing value from map" {
        val csvHeader = "City Unique ID,Tenure Min, Tenure Max"
        val csvRecords = listOf("\"my, mumbai\",,\"20\"")

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 1
        assertSoftly {
            dataRecords[0]["City Unique ID"] shouldBe "my, mumbai"
            dataRecords[0].shouldNotContainKey("Tenure Min")
            dataRecords[0]["Tenure Max"] shouldBe "20"
        }
    }
    "handle values as array" {
        val csvHeader = "City Unique ID,Pincode,Pincode,Pincode"
        val csvRecords = listOf("mumbai,400030,400031,400032")

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 1
        assertSoftly {
            dataRecords[0]["City Unique ID"] shouldBe "mumbai"
            dataRecords[0]["Pincode"] shouldBe listOf("400030", "400031", "400032")
        }
    }

    "handle values as array with blanks" {
        val csvHeader =
            "City Unique ID,(rates) Tenure Min,(rates) Tenure Max,(rates) Rate,(rates) Tenure Min,(rates) Tenure Max,(rates) Rate,(rates) Tenure Min,(rates) Tenure Max,(rates) Rate"
        val csvRecords = listOf("mumbai,1,20,9.5,21,,9.45,41,100,9.35")

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 1
        assertSoftly {
            dataRecords[0]["City Unique ID"] shouldBe "mumbai"
            val rates = dataRecords[0]["rates"] as List<Map<String, String>>
            rates[0]["Tenure Min"] shouldBe "1"
            rates[0]["Tenure Max"] shouldBe "20"
            rates[0]["Rate"] shouldBe "9.5"

            rates[1]["Tenure Min"] shouldBe "21"
            rates[1].shouldNotContainKey("Tenure Max")
            rates[1]["Rate"] shouldBe "9.45"

            rates[2]["Tenure Min"] shouldBe "41"
            rates[2]["Tenure Max"] shouldBe "100"
            rates[2]["Rate"] shouldBe "9.35"
        }
    }

    "should generate gender mappings across different systems and environments" {
        val csvHeader = "Gender Unique ID,Display Name,(finnone) prod,(finnone) sit,(finnone) uat, (hunter) prod"
        val csvRecords = listOf(
            "female,Female,F,F,F,3",
            "male,Male,M,M,M,2",
            "other,Other,O,O,O,1",
            "unknown,Unknown,U,U,U,4"
        )

        val dataRecords = CsvDataParser(csvHeader).parse(csvRecords)

        dataRecords.size shouldBe 4
        assertSoftly {
            dataRecords[0]["Gender Unique ID"] shouldBe "female"
            dataRecords[0]["Display Name"] shouldBe "Female"
            dataRecords[0]["finnone"] shouldBe listOf(
                mapOf(
                    "prod" to "F",
                    "sit" to "F",
                    "uat" to "F"
                )
            )
            dataRecords[0]["hunter"] shouldBe listOf(
                mapOf(
                    "prod" to "3"
                )
            )
            dataRecords[1]["Gender Unique ID"] shouldBe "male"
            dataRecords[1]["Display Name"] shouldBe "Male"
            dataRecords[1]["finnone"] shouldBe listOf(
                mapOf(
                    "prod" to "M",
                    "sit" to "M",
                    "uat" to "M"
                )
            )
            dataRecords[1]["hunter"] shouldBe listOf(
                mapOf(
                    "prod" to "2"
                )
            )
            dataRecords[2]["Gender Unique ID"] shouldBe "other"
            dataRecords[2]["Display Name"] shouldBe "Other"
            dataRecords[2]["finnone"] shouldBe listOf(
                mapOf(
                    "prod" to "O",
                    "sit" to "O",
                    "uat" to "O"
                )
            )
            dataRecords[2]["hunter"] shouldBe listOf(
                mapOf(
                    "prod" to "1"
                )
            )
            dataRecords[3]["Gender Unique ID"] shouldBe "unknown"
            dataRecords[3]["Display Name"] shouldBe "Unknown"
            dataRecords[3]["finnone"] shouldBe listOf(
                mapOf(
                    "prod" to "U",
                    "sit" to "U",
                    "uat" to "U"
                )
            )
            dataRecords[3]["hunter"] shouldBe listOf(
                mapOf(
                    "prod" to "4"
                )
            )
        }
    }
})