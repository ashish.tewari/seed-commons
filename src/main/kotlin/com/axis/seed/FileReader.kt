package com.axis.seed

import java.io.BufferedReader
import java.io.InputStream
import java.nio.file.FileSystems
import java.nio.file.Files
import java.util.*
import kotlin.streams.toList

class FileReader {

    fun readAsString(fileName: String): String {
        return readFileStream(fileName).readText()
    }

    fun readAsLines(fileName: String): List<String> {
        return readFileStream(fileName).readLines()
    }

    fun exists(fileName: String): Boolean {
        try {
            javaClass.getResourceAsStream(fileName) ?: CsvImporter::class.java.classLoader.getResourceAsStream(fileName)
            ?: return false
        } catch (e: Exception) {
            return false
        }
        return true
    }

    fun getAllFilesInFolder(folderURI: String): List<String> {
        //TODO("Try better approach here")
        return try {
            val uri = CsvImporter::class.java.classLoader.getResource(folderURI).toURI()
            val fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap<String, Any>())
            val resourcePath = fileSystem.getPath(folderURI)
            val files = Files.walk(resourcePath, 1).map {it.toString()}.toList().toMutableList()
            removeCurrentDirectoryFromList(files)
            files
        } catch (exception: Exception) {
            readFileStream(folderURI).readLines().map { "$folderURI/$it" }
        }
    }

    private fun removeCurrentDirectoryFromList(files: MutableList<String>) {
        files.removeAt(0)
    }

    private fun readFileStream(fileName: String): BufferedReader {
        val stream: InputStream =
            javaClass.getResourceAsStream(fileName) ?: CsvImporter::class.java.classLoader.getResourceAsStream(fileName)
        return BufferedReader(stream.reader())
    }

}