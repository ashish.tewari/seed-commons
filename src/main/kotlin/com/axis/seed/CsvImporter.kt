package com.axis.seed


class CsvImporter(private val folderName: String,
                  private val environmentAwareFileLoader: EnvironmentAwareFileLoader = EnvironmentAwareFileLoader(
                      SystemWrapperImpl()
                  )
)
    : BaseImporter(folderName, "CSV", environmentAwareFileLoader) {

    companion object {
        private const val DATA_FILE_NAME = "data.csv"
    }

    private val fileReader = FileReader()

    override fun parse(): List<Map<String, Any>> {
        val environmentSpecificFile = environmentAwareFileLoader.getFileToLoad(folderName, DATA_FILE_NAME)
        val lines = fileReader.readAsLines("/$folderName/$environmentSpecificFile")
        val csvHeader = lines[0]
        val csvRecords = lines.drop(1)
        return CsvDataParser(csvHeader).parse(csvRecords)
    }

}
