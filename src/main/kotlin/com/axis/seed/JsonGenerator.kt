package com.axis.seed

import com.axis.seed.DataConverter
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templatemode.TemplateMode
import org.thymeleaf.templateresolver.StringTemplateResolver
import java.util.stream.Stream


class JsonGenerator(private val jsonTemplate: String) {
    private val engine: TemplateEngine = TemplateEngine()

    init {
        val resolver = StringTemplateResolver()
        resolver.templateMode = TemplateMode.TEXT
        engine.addTemplateResolver(resolver)
    }

    fun generate(dataRecords: List<Map<String, Any>>): Stream<String> {
        return dataRecords.stream().parallel().map { generate(it) }
    }

    fun generate(dataRecord: Map<String, Any>): String {
        return convert(dataRecord)
    }

    private fun convert(record: Map<String, Any>): String {
        val context = Context()
        context.setVariable("props", record)
        context.setVariable("converter", DataConverter())
        return engine.process(jsonTemplate, context)
    }
}
