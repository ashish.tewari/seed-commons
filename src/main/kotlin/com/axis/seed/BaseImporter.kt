package com.axis.seed

import com.mongodb.client.model.IndexModel
import com.mongodb.client.model.IndexOptions
import org.bson.Document
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.streams.toList

abstract class BaseImporter(
    private val folderName: String,
    private val type: String,
    private val environmentAwareFileLoader: EnvironmentAwareFileLoader
    = EnvironmentAwareFileLoader(SystemWrapperImpl())
) {
    protected val logger: Logger = Logger.getLogger(javaClass.name)
    private val fileReader = FileReader()

    private val config: Document = Document.parse(fileReader.readAsString("/$folderName/config.json"))
    private val repo = DefaultMongoRepository(config)

    companion object {
        private const val RECORD_FILE_NAME = "record.json"
    }

    open fun import() {
        if (checkIfDbModificationRequired()) {
            logger.info("Importing $type: $folderName : STARTED")
            val dataRecords: List<Map<String, Any>> = parse()
            logger.info("No of records: ${dataRecords.size}")
            val jsonRecords = convert(dataRecords)
            save(jsonRecords)
            logger.info("Importing $type: $folderName : COMPLETE")
            repo.updateVersionDb("$folderName")
            logger.info("Updating ${config.getString("collectionName")}: ${config.getString("version")} : COMPLETE")
        }
    }

    protected abstract fun parse(): List<Map<String, Any>>

    protected fun convert(dataRecords: List<Map<String, Any>>): List<Document> {
        val recordFileToRead = environmentAwareFileLoader.getFileToLoad(folderName, RECORD_FILE_NAME)
        return if (recordFileToRead != null) {
            val jsonTemplate = fileReader.readAsString("/$folderName/$recordFileToRead")
            JsonGenerator(jsonTemplate).generate(dataRecords).map {
                try {
                    Document.parse(it)
                } catch (t: Throwable) {
                    logger.log(Level.SEVERE, it)
                    logger.log(Level.SEVERE, t.message, t)
                    throw t
                }
            }.toList()
        } else {
            dataRecords.stream().parallel().map {
                try {
                    Document(it)
                } catch (t: Throwable) {
                    logger.log(Level.SEVERE, it.toString())
                    logger.log(Level.SEVERE, t.message, t)
                    throw t
                }
            }.toList()
        }
    }

    protected open fun save(records: List<Document>) {
        val seedMode = config.getString("seedMode")
        if (config.containsKey("indexes")) {
            val indexConfigs = config["indexes"] as List<Document>
            addIndexes(repo, indexConfigs)
        }
        logger.info("Import mode: $seedMode")
        if (seedMode == "upsert") {
            updateInsert(records)
        } else if (seedMode == "drop-insert") {
            dropInsert(records)
        }
    }

    private fun checkIfDbModificationRequired(): Boolean {
        return !(repo.getValueFromVersionCollection("$folderName")?.get("version")?.equals(config.getString("version"))
            ?: false)
    }


    private fun addIndexes(repo: DefaultMongoRepository, indexConfigs: List<Document>) {

        val indexes = indexConfigs.map { indexConfig ->

            val indexKeysDocument = Document()

            val fields = indexConfig["fields"] as List<String>

            val type = IndexType.valueOf(indexConfig["type"] as String)
            fields.forEach { indexKeysDocument[it] = type.mappingField }

            val indexName = indexConfig["name"] as String

            val options = IndexOptions()
            options.name(indexName)


            IndexModel(indexKeysDocument, options)
        }

        repo.createIndexes(indexes)
    }

    protected fun dropInsert(records: List<Document>) {
        dropAllDocuments()
        repo.insertMany(records)
    }

    private fun dropAllDocuments() {
        repo.deleteAll(config.getValue("dropQuery") as Document)
    }

    protected fun updateInsert(records: List<Document>) {
        val keyTemplate = JsonGenerator(fileReader.readAsString("/$folderName/key.json"))
        records.forEach { repo.upsert(it, keyTemplate.generate(it)) }
    }
}