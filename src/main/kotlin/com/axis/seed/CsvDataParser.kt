package com.axis.seed

import com.opencsv.CSVParser
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.streams.toList

class CsvDataParser(csvHeader: String) {
    private val logger = Logger.getLogger(javaClass.name)

    val groupKeyRegex = Regex("\\([a-zA-Z ]*\\)")
    private val columns: List<String>
    private val parser = CSVParser()

    init {
        columns = parser.parseLine(csvHeader).toList()
    }

    fun parse(lines: List<String>): List<Map<String, Any>> {
        return lines.stream().parallel().map {
            try {
                buildDataMap(it)
            } catch (t: Throwable) {
                logger.log(Level.SEVERE, it)
                logger.log(Level.SEVERE, t.message, t)
                throw t
            }
        }.toList()
    }

    private fun buildDataMap(record: String): MutableMap<String, Any> {
        val values = parser.parseLine(record)
        val dataMap = mutableMapOf<String, Any>()
        columns.forEachIndexed(fun(index: Int, key: String) {
            val value = values[index].trim()
            if (value.isEmpty()) return@forEachIndexed

            val keyName = key.trim()
            if (groupKeyRegex.containsMatchIn(keyName)) {
                val (groupKey, keyFinal) = parseKeys(groupKeyRegex, keyName)
                parseArrayOfObjects(dataMap, groupKey, keyFinal, value)
            } else if (dataMap.containsKey(keyName)) {
                parseArrayValues(dataMap, keyName, value)
            } else {
                dataMap[keyName] = value
            }
        })
        return dataMap
    }

    private fun parseArrayOfObjects(dataMap: MutableMap<String, Any>,groupKey: String,keyFinal: String,value: String) {
        if (!dataMap.containsKey(groupKey)) dataMap[groupKey] = mutableListOf<MutableMap<String, Any>>()
        val list = dataMap[groupKey] as MutableList<MutableMap<String, Any>>
        if (list.isEmpty()) list.add(mutableMapOf(keyFinal to value))
        else {
            val last: MutableMap<String, Any> = list.last()
            if (last.containsKey(keyFinal)) list.add(mutableMapOf(keyFinal to value))
            else last[keyFinal] = value
        }
    }

    private fun parseArrayValues(dataMap: MutableMap<String, Any>,keyName: String,value: String) {
        val listOfValues: MutableList<String?> = when (dataMap[keyName]) {
            is String -> mutableListOf(dataMap[keyName] as String)
            else -> (dataMap[keyName] as MutableList<String?>)
        }
        listOfValues.add(value)
        dataMap[keyName] = listOfValues
    }

    private fun parseKeys(regex: Regex, keyName: String): Pair<String, String> {
        val groupKey = regex.find(keyName)!!.value.trim().removePrefix("(").removeSuffix(")")
        val keyFinal = keyName.removePrefix("($groupKey)").trim()
        return Pair(groupKey, keyFinal)
    }


}