package com.axis.seed

import java.util.logging.Logger


enum class Environment(val value: String) {
    TEST("-test"),
    UAT("-uat") ,
    QA("-qa") ,
    SANDBOX("-sandbox"),
    SIT("-sit") ,
    INFRA("-infra"),
    PROD("-prod"),
    AXISQA("-axisqa"),
    PERF("-perf"),
    DEFAULT("")
}

class EnvironmentAwareFileLoader(private val system: SystemWrapper, private val fileReader: FileReader = FileReader()) {

    private val logger: Logger = Logger.getLogger(javaClass.name)

    companion object {
        const val DOT = "."
        const val ACTIVE_ENVIRONMENT = "spring.profiles.active"
    }

    private val env: Environment by lazy {
        getEnvironmentToUse()
    }

    fun getFolderToLoad(folderName: String): String {
        val environmentSpecificFolder = folderName + env.value
        if (fileReader.exists(environmentSpecificFolder)) {
            return environmentSpecificFolder
        }
        return folderName
    }

    fun getFileToLoad(folderName: String, defaultFileName: String): String? {
        val filePrefix = defaultFileName.split(".")[0]
        val fileSuffix = defaultFileName.split(".")[1]

        val envSpecificFileName = filePrefix + env.value + DOT + fileSuffix
        if (fileExistsInAFolder(folderName, envSpecificFileName)) return envSpecificFileName
        if (fileExistsInAFolder(folderName, defaultFileName)) return defaultFileName
        return null
    }

    private fun fileExistsInAFolder(folderName: String, envSpecificFileName: String) =
        fileReader.exists("/$folderName/$envSpecificFileName")

    private fun getEnvironmentToUse(): Environment {
        val environmentVariable = system.getenv(ACTIVE_ENVIRONMENT)
        logger.info("Value of env variable set $environmentVariable")
        var environment = Environment.DEFAULT
        if (environmentVariable != null) {
            environment = Environment.valueOf(environmentVariable.toUpperCase())
        }
        logger.info("Using ${environment.name} environment")
        return environment
    }
}
