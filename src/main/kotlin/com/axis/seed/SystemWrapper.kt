package com.axis.seed

interface SystemWrapper {
    fun getenv(name: String): String?
}

class SystemWrapperImpl: SystemWrapper {
    override fun getenv(name: String): String? {
        return System.getenv(name)
    }
}