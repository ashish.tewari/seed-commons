package com.axis.seed

import com.mongodb.BasicDBObject
import com.mongodb.client.MongoClients
import com.mongodb.client.model.IndexModel
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.UpdateOptions
import com.mongodb.client.result.DeleteResult
import org.bson.Document
import java.util.logging.Level
import java.util.logging.Logger


class DefaultMongoRepository(private val config: Document) {
    private val logger = Logger.getLogger(javaClass.name)

    private val client = MongoClients.create(System.getenv("mongodb.url") ?: "mongodb://localhost:27017")
    private val database = client.getDatabase(config.getString("databaseName"))

    fun deleteAll(document: Document) {
        val collection = database.getCollection(config.getString("collectionName"))
        val result: DeleteResult = collection.deleteMany(document)
        logger.info("No of records deleted: ${result.deletedCount}")
    }

    fun upsert(document: Map<String, Any>, key: String) {
        val options = UpdateOptions().upsert(true)
        try {
            val collection = database.getCollection(config.getString("collectionName"))
            collection.updateOne(BasicDBObject.parse(key), BasicDBObject("\$set", document), options)
        } catch (t: Throwable) {
            logger.log(Level.SEVERE, document.toString())
            logger.log(Level.SEVERE, t.message, t)
        }
    }

    fun dataAlreadyPresent(): Boolean {
        val collection = database.getCollection(config.getString("collectionName"))
        return collection.countDocuments(config.getValue("dropQuery") as Document) > 0
    }

    fun createIndexes(indexes: List<IndexModel>) {
        val collection = database.getCollection(config.getString("collectionName"))
        collection.createIndexes(indexes)
    }

    fun insertMany(docs: List<Document>) {
        val collection = database.getCollection(config.getString("collectionName"))
        collection.insertMany(docs)
//        docs.forEach {
//            try {
//                collection.insertOne(it)
//            } catch (t: Exception) {
//                logger.log(Level.SEVERE, it.toJson())
//                logger.log(Level.SEVERE, t.message, t)
//            }
//        }
    }

    fun getValueFromVersionCollection(parent: String): Document? {
        val collection = database.getCollection("version_log")
        return collection.find(Document("collectionName", "$parent-" + config.getString("collectionName"))).first()
    }

    fun updateVersionDb(parent: String) {
        val collection = database.getCollection("version_log")

        collection.createIndex(Document("collectionName", 1), IndexOptions().unique(true))
        val versionJson: Map<String, String> = mapOf(
            Pair("collectionName", "$parent-" + config.getString("collectionName")),
            Pair("version", config.getString("version"))
        )
        collection.updateOne(
            Document("collectionName", "$parent-" + config.getString("collectionName")),
            BasicDBObject("\$set", versionJson),
            UpdateOptions().upsert(true)
        )
    }
}
