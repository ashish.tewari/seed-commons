package com.axis.seed

import java.util.*

class DataConverter {

    private val fileReader = FileReader()

    fun toUniqueId(value: String) = value.toLowerCase()

    fun fileAsBase64(fileName: String): String {
        val readAsString: String = fileReader.readAsString(fileName)
        return Base64.getEncoder().encodeToString(readAsString.toByteArray())
    }
}