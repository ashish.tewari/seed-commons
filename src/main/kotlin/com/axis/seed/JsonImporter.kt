package com.axis.seed

import com.fasterxml.jackson.databind.ObjectMapper

class JsonImporter(private val folderName: String,
                   private val environmentAwareFileLoader: EnvironmentAwareFileLoader = EnvironmentAwareFileLoader(
                       SystemWrapperImpl()
                   )
): BaseImporter(folderName, "JSON") {

    companion object {
        private const val DATA_FILE_NAME = "data.json"
    }

    private val fileReader = FileReader()

    override fun parse(): List<Map<String, Any>> {
        val mapper = ObjectMapper()
        val typeReference = mapper.getTypeFactory().constructCollectionType(List::class.java, HashMap::class.java)
        val environmentSpecificFile = environmentAwareFileLoader.getFileToLoad(folderName, DATA_FILE_NAME)
        val dataRecords = fileReader.readAsString("/$folderName/$environmentSpecificFile")
        return mapper.readValue(dataRecords, typeReference)
    }
}
