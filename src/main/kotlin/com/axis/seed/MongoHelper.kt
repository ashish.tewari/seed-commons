package com.axis.seed

import com.axis.seed.DefaultMongoRepository
import com.axis.seed.FileReader
import org.bson.Document

class MongoHelper {
    companion object {
        fun dataPresentForFolder(folderName: String): Boolean {
            val fileReader = FileReader()
            val config = Document.parse(fileReader.readAsString("/$folderName/config.json"))
            val repo = DefaultMongoRepository(config)
            return repo.dataAlreadyPresent()
        }
    }
}