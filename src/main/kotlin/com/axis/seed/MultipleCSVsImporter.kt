package com.axis.seed

import org.bson.Document

class MultipleCSVsImporter(
    private val folderName: String,
    private val environmentAwareFileLoader: EnvironmentAwareFileLoader = EnvironmentAwareFileLoader(SystemWrapperImpl())
) : BaseImporter(folderName, "CSV", environmentAwareFileLoader) {

    companion object {
        const val CSV_FOLDER = "data"
    }
    private val fileReader = FileReader()
    private val config = Document.parse(fileReader.readAsString("/$folderName/config.json"))
    private val repo = DefaultMongoRepository(config)


    override fun import() {
        logger.info("Importing CSV: $folderName : STARTED")
         val dataFolderURI = "$folderName/$CSV_FOLDER"
        val environmentSpecificFolder = environmentAwareFileLoader.getFolderToLoad(dataFolderURI)
        val csvFiles = fileReader.getAllFilesInFolder(environmentSpecificFolder)
        val csvHeader = extractCSVHeaderFromFirstCSVFile(csvFiles)
        var totalRecords = 0
        csvFiles.forEachIndexed { index, csvFile ->
            val csvRecords = readRecordsFromFile(csvFile)
            val parsedRecord = CsvDataParser(csvHeader).parse(csvRecords)
            val jsonRecords = convert(parsedRecord)
            save(jsonRecords, index)
            totalRecords += jsonRecords.count()
        }
        logger.info("No of records: $totalRecords")
        logger.info("Importing CSV: $folderName : COMPLETE")
    }

    override fun parse(): List<Map<String, Any>> {
        TODO("Not being used " +
                "| Because of the inheritance constraints we had to extend from Base Importer" +
                "| Ideally it should be Composition to make reuse ")
    }

    private fun readRecordsFromFile(csvFile: String): List<String> {
        val lines = fileReader.readAsLines(csvFile)
        return lines.drop(1)
    }

    private fun extractCSVHeaderFromFirstCSVFile(csvFiles: List<String>): String {
        val firstCSVFile = csvFiles.first()
        val lines = fileReader.readAsLines(firstCSVFile)
        return lines[0]
    }

    private fun save(records: List<Document>, csvFileIndex: Int) {
        val seedMode = config.getString("seedMode")
        if (isFirstCSVFileRecordsBeingInserted(csvFileIndex)) logger.info("Import mode: $seedMode")
        if (seedMode == "upsert") {
            updateInsert(records)
        } else if (seedMode == "drop-insert" && isFirstCSVFileRecordsBeingInserted(csvFileIndex)) {
            dropInsert(records)
        } else if(seedMode == "drop-insert" && csvFileIndex > 0) {
            repo.insertMany(records)
        }
    }

    private fun isFirstCSVFileRecordsBeingInserted(index: Int) = index == 0
}